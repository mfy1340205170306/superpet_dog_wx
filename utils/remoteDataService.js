import { HttpService, HttpUploadService } from '../utils/httpService';
const RemoteDataService = {
  wxLogin(params) {
    return HttpService('/login/doLogin', params);
  },
  getWeRunStep(params){
    return HttpService('/wxrun/getWeRunData', params);
  },
  updateUserInfo(params) {
    return HttpService('/user/updateUser', params);
  },
  savePetInfo(params) {
    return HttpService('/pet/savePet', params);
  },
  getUserPetList(params) {
    return HttpService('/pet/getUserPetList', params);
  },
  getSomeonePetList(params) {
    return HttpService('/pet/getSomeonePetList', params);
  },
  getUserPetDet(params) {
    return HttpService('/pet/getUserPetDet', params);
  },
  getUserPetFeedByDayList(params) {
    return HttpService('/pet/getUserPetFeedByDayList', params);
  },
  feedPet(params) {
    return HttpService('/pet/feedPet', params);
  },
  getUserPetPicList(params) {
    return HttpService('/pet/getUserPetPicList', params);
  },
  deletePetPic(params) {
    return HttpService('/pet/deletePetPic', params);
  },
  CollectPet(params) {
    return HttpService('/pet/CollectPet', params);
  },
  likePetPic(params) {
    return HttpService('/pet/likePetPic', params);
  },
  getPetHouseList(params) {
    return HttpService('/pet/getPetHouseList', params);
  },
  getFeedPetList(params) {
    return HttpService('/pet/getFeedPetList', params);
  },
  getPetCardBg(params){
    return HttpService('/pet/getPetCardBg', params);
  },

  uploadPetAvatar(params, filePath, fileName, completeCallback){
    return HttpUploadService('/fileUploadAction', params, filePath, fileName, completeCallback);
  }
}
module.exports = {
  RemoteDataService: RemoteDataService 
}