import { RemoteDataService } from '../../utils/remoteDataService.js';
import { UserStorage } from "../../utils/storageSevice.js";
import { Router } from "../../utils/common.js";

Page({

  data: {
    isMyFeed: true,
    myFeedList: [],
    feedMyList: []
  },

  onLoad: function (options) {
    let that = this
    let params = {}
    wx.showNavigationBarLoading()
    RemoteDataService.getFeedPetList(params).then(result => {
      if (result && result.code == "000") {
        that.setData({
          myFeedList: result.myFeed,
          feedMyList: result.feedMy
        })
      }
      wx.hideNavigationBarLoading()
    }).catch(err => {
      wx.hideNavigationBarLoading()
    })
  },
  clickTab: function(){
    let that = this
    that.setData({
      isMyFeed: !that.data.isMyFeed
    })
  },
  navToPetDet: function (e) {
    let params = {
      petId: e.currentTarget.dataset.petid
    }
    Router.navigateTo("../dogdet/dogdet", params);
  },
  navToSomeonePet: function (e) {
    let currUserId = e.currentTarget.dataset.userid
    let userInfo = UserStorage.getData()
    if (currUserId == userInfo.userId) {
      wx.switchTab({
        url: "/pages/index/index"
      })
    } else {
      let params = {
        userId: currUserId
      }
      Router.navigateTo("../doglist/doglist", params)
    }
  }

})